<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>

<div class="map-points__insert">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <a class="map-point">
            <i class="map-point__icon"></i>
            <span class="map-point__name"><?= $arItem["NAME"] ?></span>
            <div class="map-point__adress"><?= $arItem['PROPERTIES']['ADDRESS']['~VALUE']['TEXT'] ?></div>
        </a>

    <? endforeach; ?>

</div>

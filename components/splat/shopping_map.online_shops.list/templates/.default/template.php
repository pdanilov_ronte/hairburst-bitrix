<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>

<div class="map-points__brands map-brands">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <a href="<?= $arItem['PROPERTIES']['ADDRESS_URL']['VALUE'] ?>" class="map-brands__item" target="_blank">
            <div class="map-brands__pic" style="background-image: url(<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>);"></div>
            <div class="map-brands__title"><?= $arItem['NAME'] ?></div>
            <div class="map-brands__desc"><?= $arItem['PREVIEW_TEXT'] ?></div>
            <div class="map-brands__link"><?= $arItem['PROPERTIES']['ADDRESS_TITLE']['VALUE'] ?></div>
        </a>

    <? endforeach; ?>

</div>

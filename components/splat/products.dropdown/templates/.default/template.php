<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>

    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="product-dropdown__item">
        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" width="100"
             class="product-dropdown__item__img">
        <span class="product-dropdown__item__title"><?= $arItem["NAME"] ?></span>
        <span class="product-dropdown__item__desc"><?= $arItem['PROPERTIES']['SHORT_DESCRIPTION']['VALUE'] ?></span>

    </a>

<? endforeach; ?>

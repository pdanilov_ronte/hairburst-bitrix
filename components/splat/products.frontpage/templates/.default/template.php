<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>

<div class="frontfeatures">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>

        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" target="_blank" class="frontfeatures__item"
           id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <span class="frontfeatures__category"></span>
            <span class="frontfeatures__imageplace">
			<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                 class="frontfeatures__image">
		</span>
            <span class="frontfeatures__title"><?= $arItem["NAME"] ?></span>
            <span class="frontfeatures__desc"><?= $arItem['PROPERTIES']['SHORT_DESCRIPTION']['VALUE'] ?></span>
            <? if (!empty($arItem['PROPERTIES']['HOMEPAGE_LABEL']['VALUE'])): ?>
                <span class="frontfeatures__label"><?= $arItem['PROPERTIES']['HOMEPAGE_LABEL']['VALUE'] ?></span>
            <? endif; ?>
        </a>

    <? endforeach; ?>

</div>

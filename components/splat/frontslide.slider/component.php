<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

use Bitrix\Main\Context,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Loader,
    Bitrix\Iblock;

if ($this->startResultCache()) {

    if (!Loader::includeModule("iblock")) {
        $this->abortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $rsIBlock = CIBlock::GetList(array(), array(
        "ACTIVE" => "Y",
        "ID" => $arParams["IBLOCK_ID"],
    ));

    if ($arResult = $rsIBlock->GetNext()) {

        $arSelect = array_merge($arParams["FIELD_CODE"], array(
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "DETAILS_PICTURE"
        ));

        $bGetProperty = count($arParams["PROPERTY_CODE"])>0;
        if($bGetProperty)
            $arSelect[]="PROPERTY_*";

        $arFilter = array(
            "IBLOCK_ID" => $arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "N",
        );

        $arResult["SECTION"] = false;

        $arSort = array(
            $arParams["SORT_BY1"] => $arParams["SORT_ORDER1"]
        );
        if (!array_key_exists("ID", $arSort))
            $arSort["ID"] = "ASC";

        $obParser = new CTextParser;
        $arResult["ITEMS"] = array();
        $arResult["ELEMENTS"] = array();
        $rsElement = CIBlockElement::GetList($arSort, $arFilter);
        $rsElement->SetUrlTemplates($arParams["DETAIL_URL"], "", $arParams["IBLOCK_URL"]);
        while ($obElement = $rsElement->GetNextElement()) {

            $arItem = $obElement->GetFields();

            $ipropValues = new Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
            $arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

            Iblock\Component\Tools::getFieldImageData(
                $arItem,
                array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
                Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
                'IPROPERTY_VALUES'
            );

            $arItem["PROPERTIES"] = $obElement->GetProperties();

            /*if($bGetProperty)
                $arItem["PROPERTIES"] = $obElement->GetProperties();
            $arItem["DISPLAY_PROPERTIES"]=array();
            foreach($arParams["PROPERTY_CODE"] as $pid)
            {
                $prop = &$arItem["PROPERTIES"][$pid];
                if(
                    (is_array($prop["VALUE"]) && count($prop["VALUE"])>0)
                    || (!is_array($prop["VALUE"]) && strlen($prop["VALUE"])>0)
                )
                {
                    $arItem["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arItem, $prop, "news_out");
                }
            }*/

            $arResult["ITEMS"][] = $arItem;
            $arResult["ELEMENTS"][] = $arItem["ID"];
        }
    }
    else
    {
        $this->abortResultCache();
        Iblock\Component\Tools::process404(
            trim($arParams["MESSAGE_404"]) ?: 'Invalid component configuration (frontslide.slider)'
            ,true
            ,$arParams["SET_STATUS_404"] === "Y"
            ,$arParams["SHOW_404"] === "Y"
            ,$arParams["FILE_404"]
        );
    }
}

$this->includeComponentTemplate();
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>
<!--<h1>Slider</h1>-->
<?php //print '<pre>'; print_r($arResult); exit; ?>

<div class="frontslider-height">
    <div class="frontslider fotorama" data-role="frontslider" data-fit="cover" data-width="100%" data-height="650"
         data-margin="0" data-autoplay="3000">

        <? foreach ($arResult["ITEMS"] as $arItem): ?>

            <div class="frontslider__slide" data-img="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>">
                <div class="frontslider__wrapper" style="position: relative; top: 220px;">
                    <? if (!empty($arItem['PROPERTIES']['QUOTE']['~VALUE']['TEXT'])): ?>
                        <div class="frontslider__quote"><?= $arItem['PROPERTIES']['QUOTE']['~VALUE']['TEXT'] ?></div>
                    <? endif; ?>
                    <? if (!empty($arItem['PROPERTIES']['AUTHOR']['VALUE'])): ?>
                        <div class="frontslider__author"><?= $arItem['PROPERTIES']['AUTHOR']['VALUE'] ?></div>
                    <? endif; ?>
                    <? if (!empty($arItem['PROPERTIES']['CATEGORY']['VALUE'])): ?>
                        <div class="frontslider__category"><?= $arItem['PROPERTIES']['CATEGORY']['VALUE'] ?></div>
                    <? endif; ?>
                    <? if (!empty($arItem['PROPERTIES']['HEADER']['~VALUE']['TEXT'])): ?>
                        <div class="frontslider__header"><?= $arItem['PROPERTIES']['HEADER']['~VALUE']['TEXT'] ?></div>
                    <? endif; ?>
                    <? if (!empty($arItem['PROPERTIES']['LINK']['~VALUE']['TEXT'])): ?>
                        <div class="frontslider__link"><?= $arItem['PROPERTIES']['LINK']['~VALUE']['TEXT'] ?></div>
                    <? endif; ?>
                </div>
            </div>

        <? endforeach; ?>

    </div>
</div>

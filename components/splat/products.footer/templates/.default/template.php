<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
?>

<div class="footer-menu__link footer-menu__title">Продукты</div>
<ul class="footer-list">

    <? foreach ($arResult["ITEMS"] as $arItem): ?>

        <li class="footer-list__item">
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="footer-menu__link"><?= $arItem["NAME"] ?></a>
        </li>

    <? endforeach; ?>

</ul>

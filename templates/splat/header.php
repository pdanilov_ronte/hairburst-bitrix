<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width">
    <meta name="robots" content="noindex,nofollow"/>
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="robots" content="index, follow"/>
    <meta name="keywords" content="Splat сплат зубные пасты пенки ополаскиватели нити щётки"/>
    <meta name="description"
          content="Официальный производитель продукции SPLAT. Производство, зубных паст, зубных щеток, средств гигиены и защиты полости рта"/>
    <link href="/bitrix/js/main/core/css/core.css?14679224722953" type="text/css" rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/index.css?14679224687761" type="text/css" rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/normalize.css?14679224681880" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/fonts/fonts.css?14679224684189" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/fotorama.css?146792246838810" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/style.css?1470334766123508" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/css/animations.css?146792246815182" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <link href="<?= SITE_TEMPLATE_PATH ?>/styles.css?146858204217028" type="text/css" data-template-style="true"
          rel="stylesheet"/>
    <script type="text/javascript">if (!window.BX)window.BX = {
            message: function (mess) {
                if (typeof mess == 'object') for (var i in mess) BX.message[i] = mess[i];
                return true;
            }
        };</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'JS_CORE_LOADING': 'Загрузка...',
            'JS_CORE_NO_DATA': '- Нет данных -',
            'JS_CORE_WINDOW_CLOSE': 'Закрыть',
            'JS_CORE_WINDOW_EXPAND': 'Развернуть',
            'JS_CORE_WINDOW_NARROW': 'Свернуть в окно',
            'JS_CORE_WINDOW_SAVE': 'Сохранить',
            'JS_CORE_WINDOW_CANCEL': 'Отменить',
            'JS_CORE_H': 'ч',
            'JS_CORE_M': 'м',
            'JS_CORE_S': 'с',
            'JSADM_AI_HIDE_EXTRA': 'Скрыть лишние',
            'JSADM_AI_ALL_NOTIF': 'Показать все',
            'JSADM_AUTH_REQ': 'Требуется авторизация!',
            'JS_CORE_WINDOW_AUTH': 'Войти',
            'JS_CORE_IMAGE_FULL': 'Полный размер'
        });</script>
    <script type="text/javascript">(window.BX || top.BX).message({
            'LANGUAGE_ID': 'ru',
            'FORMAT_DATE': 'DD.MM.YYYY',
            'FORMAT_DATETIME': 'DD.MM.YYYY HH:MI:SS',
            'COOKIE_PREFIX': 'BITRIX_SM',
            'SERVER_TZ_OFFSET': '10800',
            'SITE_ID': 's1',
            'USER_ID': '',
            'SERVER_TIME': '1470549022',
            'USER_TZ_OFFSET': '0',
            'USER_TZ_AUTO': 'Y',
            'bitrix_sessid': '9fefeb1333fa7eeb0c52864ccb52609d'
        });</script>


    <script type="text/javascript" src="/bitrix/js/main/core/core.js?146792247285675"></script>
    <script type="text/javascript" src="/bitrix/js/main/core/core_ajax.js?146792247226714"></script>


    <script type="text/javascript" src="/local/components/splat/search.title/script.js?14679224716161"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.min.js?146792246895786"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/modernizr.min.js?146792246811084"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/fotorama.js?146792246863081"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/ikselect.js?146792246817354"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/owl.carousel.js?146792246829933"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/doT/doT.min.js?14679224683333"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/moment/min/moment.min.js?146792246832364"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/social-likes.js?14679224689829"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.socialfeed.js?146792246815502"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/swiper.js?146792246869804"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/script.js?146792246835516"></script>
    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/sticky.js?14679224682533"></script>


    <title><? $APPLICATION->ShowTitle() ?></title>
    <link rel="canonical" href="http://www.splat.ru/"/>
    <? $APPLICATION->ShowHead() ?>
</head>
<body>

<? $APPLICATION->ShowPanel(); ?>

<!-- HEADER START -->
<script>
    var LANG_DIR = '/';
</script>
<div id="panel"></div>
<a href="/" class="logo"></a>
<header class="header header_white<?= isset($arDirProperties['HEADER_CLASS']) ? ' header_' . $arDirProperties['HEADER_CLASS'] : ''; ?>">
    <div class="header__left">

        <nav class="top-nav">
            <?
            $APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                "ROOT_MENU_TYPE" => "top",
                "MAX_LEVEL" => "1",
                "CHILD_MENU_TYPE" => "left",
                "USE_EXT" => "N",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => "",
            ),
                false
            );
            ?>
        </nav>
    </div>
    <div class="header__right">
        <a class="header_cart__link" href="/shopping-map/" target="_self"></a>

        <!--<div class="language-wrap">

            <a href="/?lang=ru" class="language language_ru">RU</a>

            <div class="language-wrap-list">
                <a href="/en/?lang=en" class="language language_en">EN</a>
            </div>
        </div>-->

        <!--<a class="top-search__link" data-role="topSearchTrigger"></a>-->
    </div>
    <form class="top-search" id="search" action="/catalog/" data-role="topSearchBlock">
        <submit name="s" type="submit" class="top-search__btn"></submit>
        <input name="q" placeholder="Что ищем?" class="top-search__input" data-role="topSearchInput"
               id="title-search-input" value="" autocomplete="off">
    </form>
    <script>
        BX.ready(function () {
            new JCTitleSearch({
                'AJAX_PAGE': '/',
                'CONTAINER_ID': 'search',
                'INPUT_ID': 'title-search-input',
                'MIN_QUERY_LEN': 2
            });
        });
    </script>


</header>
<!-- HEADER END -->
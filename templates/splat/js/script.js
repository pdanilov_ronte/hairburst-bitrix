function runAnimationSection(e) {
    e.find(".animation-block").each(function() {
        runAnimationBlock($(this))
    })
}

function runAnimationBlock(e) {
    var t = parseInt(e.data("animation-timestart")),
        o = 100;
    isNaN(t) && (t = 0);
    var i = e.data("animation-class");
    e.find(".animation").each(function() {
        var e = $(this);
        setTimeout(function() {
            e.addClass(i)
        }, t), t += o
    })
}(function() {
    var e, t;
    t = function(e, t, o) {
        var i;
        return (i = e[t]) ? (e[t] = function() {
            return arguments[o] = arguments[o].replace(/@([\w\u00c0-\uFFFF\-]+)/g, '[data-role~="$1"]'), i.apply(e, arguments)
        }, $.extend(e[t], i)) : void 0
    }, t($, "find", 0), t($, "multiFilter", 0), t($.find, "matchesSelector", 1), t($.find, "matches", 0), e = function(e, t) {
        var o, i, a, s, n;
        for (a = $.trim(e).split(/\s+/), o = [], s = 0, n = a.length; n > s; s++) i = a[s], ~$.inArray(i, o) || t && ~$.inArray(i, t) || o.push(i);
        return o
    }, $.extend($.fn, {
        roles: function() {
            return e(this.attr("data-role"))
        },
        hasRole: function(t) {
            var o, i, a, s;
            for (i = e(t), a = 0, s = i.length; s > a; a++)
                if (o = i[a], !this.is("@" + o)) return !1;
            return !0
        },
        addRole: function(t) {
            return this.hasRole(t), this.each(function(o, i) {
                var a, s, n;
                return a = $(i), s = a.attr("data-role"), n = s ? s + " " + t : t, a.attr("data-role", e(n).join(" "))
            })
        },
        removeRole: function(t) {
            return this.hasRole(t) ? this.each(function(o, i) {
                var a, s;
                a = $(i), s = e(a.attr("data-role"), e(t)).join(" "), a.attr("data-role", s)
            }) : this
        },
        toggleRole: function(t) {
            var o, i, a;
            for (a = e(t), i = 0; i < a.length;) o = this.hasRole(a[i]) ? "removeRole" : "addRole", this[o].call(this, a[i]), i++;
            return this
        }
    })
}).call(this), $(document).ready(function() {
        is_chrome = navigator.userAgent.indexOf("Chrome") > -1, is_explorer = navigator.userAgent.indexOf("MSIE") > -1, is_firefox = navigator.userAgent.indexOf("Firefox") > -1, is_safari = navigator.userAgent.indexOf("Safari") > -1, is_opera = navigator.userAgent.indexOf("Presto") > -1, is_mac = -1 != navigator.userAgent.indexOf("Mac OS"), is_windows = !is_mac, is_chrome && is_safari && (is_safari = !1), (is_safari || is_windows) && $("body").css("-webkit-text-stroke", "0.15px")
    }),
    function() {
        window.splat = {}, $(function() {
            splat.instagramFeedLength = 20, splat.updateInstagramFeed = function() {
                $("@instagramFeed").socialfeed({
                    instagram: {
                        accounts: ["@splat_global"],
                        limit: splat.instagramFeedLength,
                        client_id: "7b758734bc2a401797f5cd34811aff81"
                    },
                    template: "/local/templates/splat/instagram-feed-item.html",
                    length: 200,
                    show_media: !0,
                    callback: function() {}
                })
            }, splat.updateInstagramFeed()
        }), $(function() {
            $(".about-slider").owlCarousel({
                navigation: !0,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: !0,
                autoHeight: !0,
                transitionStyle: "fade"
            })
        }), $(function() {
            splat.facebookFeedLength = 10, splat.updateFacebookFeed = function() {
                $("@facebookFeed").socialfeed({
                    facebook: {
                        accounts: ["@splat.ru"],
                        limit: splat.facebookFeedLength,
                        access_token: "150849908413827|a20e87978f1ac491a0c4a721c961b68c"
                    },
                    template: "/local/templates/splat/facebook-feed-item.php",
                    length: 100,
                    show_media: !0,
                    moderation: function(e) {
                        return e.text ? -1 == e.text.indexOf("fuck") : !0
                    },
                    callback: function() {
                        $(".social-likes").socialLikes()
                    }
                })
            }, splat.updateFacebookFeed()
        }), $(function() {
            $(".social-likes").on("ready.social-likes", function(e) {
                $block = $(e.currentTarget), console.log($block), $hiddenWidgets = $block.find(".social-likes__widget").eq(1).nextAll(), $hiddenWidgets.wrapAll('<div class="social-likes__popup" data-role="socialPopup"></div>'), $block.addClass("is-visible")
            })
        }), $(function() {
            onScrollerInit = function(e) {
                $prev = e.wrapper.siblings(".swiper-button-prev").addClass("is-disabled"), $next = e.wrapper.siblings(".swiper-button-next"), prevent = !1, $next.on("click.next", function() {
                    prevent || $next.hasClass("is-disabled") || (e.slideNext(), prevent = !0), setTimeout(function() {
                        prevent = !1
                    }, 450)
                }), $prev.on("click.prev", function() {
                    prevent || $prev.hasClass("is-disabled") || (e.slidePrev(), prevent = !0), setTimeout(function() {
                        prevent = !1
                    }, 300)
                }), setTimeout(function() {
                    setDisabledSlides(e, !0)
                }, 400)
            }, setDisabledSlides = function(e, t) {
                t = t || !1, i = e.activeIndex, slidesAmount = e.slides.length, slidesPerColumn = e.params.slidesPerColumn, spaceWidth = e.params.spaceBetween, slideWidth = e.slidesSizesGrid[0], visibleSlides = Math.floor((1220 + spaceWidth) / (slideWidth + spaceWidth)), leftLimit = ($(window).width() - 1220) / 2, rightLimit = leftLimit + 1220 - slideWidth + 10, distance = slideWidth + spaceWidth, e.previousIndex < i ? futureDistance = -distance : e.previousIndex > i ? futureDistance = distance : futureDistance = 0, t && (futureDistance = 0), e.slides.each(function(e, t) {
                    $slide = $(t), left = $slide.offset().left + futureDistance, left < leftLimit || left > rightLimit ? $slide.addClass("is-disabled") : $slide.removeClass("is-disabled")
                })
            }, setDisabledButtons = function(e) {
                i = e.activeIndex, slidesAmount = e.slides.length, slidesPerColumn = e.params.slidesPerColumn, spaceWidth = e.params.spaceBetween, slideWidth = e.slidesSizesGrid[0], visibleSlides = Math.floor((1220 + spaceWidth) / (slideWidth + spaceWidth)), possibleSteps = slidesAmount / slidesPerColumn - visibleSlides, $prev = e.wrapper.siblings(".swiper-button-prev"), $next = e.wrapper.siblings(".swiper-button-next"), 0 == i ? $prev.addClass("is-disabled") : $prev.removeClass("is-disabled"), i >= possibleSteps ? $next.addClass("is-disabled") : $next.removeClass("is-disabled")
            }, onScrollerTransition = function(e) {
                i = e.activeIndex, (e.previousIndex > i || e.previousIndex < i) && setDisabledSlides(e), setDisabledButtons(e)
            }, defaultOptions = {
                slidesPerView: "auto",
                grabCursor: !1,
                setWrapperSize: !0,
                slidesPerColumn: 1,
                simulateTouch: !1,
                centeredSlides: !0,
                onInit: function(e) {
                    onScrollerInit(e)
                },
                onTransitionStart: function(e) {
                    onScrollerTransition(e)
                }
            }, waitForLoading = function(e, t) {
                "facebook" == e && (length = splat.facebookFeedLength, counter = splat.facebookCounter), "instagram" == e && (length = splat.instagramFeedLength, counter = splat.instagramCounter), counter >= length ? t() : setTimeout(function() {
                    waitForLoading(e, t)
                }, 100)
            }, $facebookScroller = $("@facebookScroller"), $facebookScroller[0] && waitForLoading("facebook", function() {
                options = $.extend(defaultOptions, {
                    scrollbar: $facebookScroller.find(".swiper-scrollbar"),
                    scrollbarHide: !1,
                    slidesPerView: "auto",
                    spaceBetween: 40,
                    centeredSlides: !0
                }), facebookScroller = new Swiper($facebookScroller, options)
            }), $instagramScroller = $("@instagramScroller"), $instagramScroller[0] && waitForLoading("instagram", function() {
                setTimeout(function() {
                    options = $.extend({}, defaultOptions, {
                        scrollbar: $instagramScroller.find(".swiper-scrollbar"),
                        scrollbarHide: !1,
                        slidesPerColumn: 2,
                        spaceBetween: 20,
                        centeredSlides: !0
                    }), instagramScroller = new Swiper($instagramScroller, options)
                }, 300)
            }), $productsScroller = $("@productsScroller"), $productsScroller[0] && (options = $.extend({}, defaultOptions, {
                scrollbar: $productsScroller.find(".swiper-scrollbar"),
                scrollbarHide: !1,
                spaceBetween: 40,
                centeredSlides: !0
            }), productsScroller = new Swiper($productsScroller, options)), $productionScroller = $("@productionScroller"), $productionScroller[0] && (options = $.extend({}, defaultOptions, {
                spaceBetween: 40,
                centeredSlides: !1
            }), productionScroller = new Swiper($productionScroller, options)), $frontTicketsScroller = $("@frontlettersScroller"), $frontTicketsScroller[0] && (options = $.extend({}, defaultOptions, {
                spaceBetween: 40
            }), frontTicketsScroller = new Swiper($frontTicketsScroller, options)), $ticketsScroller = $("@ticketsScroller"), $ticketsScroller[0] && (options = $.extend({}, defaultOptions, {
                spaceBetween: 40,
                onInit: function(e) {
                    onScrollerInit(e), $yearLinks = e.wrapper.siblings("@years").find("@yearLink"), isActiveClass = "list-years__link_active", $yearLinks.on("click", function() {
                        $this = $(this), $yearLinks.removeClass(isActiveClass), $this.addClass(isActiveClass), year = $this.attr("data-year"), i = e.slides.filter('[data-year="' + year + '"]').first().index() - 1, e.slideTo(i), setTimeout(function() {
                            setDisabledSlides(e, !0)
                        }, 400)
                    })
                }
            }), ticketsScroller = new Swiper($ticketsScroller, options))
        }), $(function() {
            function e(e) {
                $.ajax({
                    url: "?",
                    type: "POST",
                    data: e.serialize()
                }).done(function() {
                    $("@reviewStateChoice").removeClass("is-visible"), $("@reviewStateRegistration").removeClass("is-visible"), $("@reviewStateDone").addClass("is-visible")
                })
            }
            var t, o, i, a, n;
            $(".catalog-block_hovered").each(function() {
                t = $(this).find(".catalog-block__img"), $(t[0]).addClass("catalog-block__img_on")
            }), $(".catalog-block_hovered").hover(function() {
                function e() {
                    i = window.setTimeout(function() {
                        $(t[1]).addClass("catalog-block__img_on").siblings().removeClass("catalog-block__img_on")
                    }, 200), a = window.setTimeout(function() {
                        $(t[2]).addClass("catalog-block__img_on").siblings().removeClass("catalog-block__img_on")
                    }, 2e3), n = window.setTimeout(function() {
                        $(t[0]).addClass("catalog-block__img_on").siblings().removeClass("catalog-block__img_on")
                    }, 4e3)
                }
                t = $(this).find(".catalog-block__img"), e(), o = window.setInterval(function() {
                    e()
                }, 6e3)
            }, function() {
                window.clearInterval(o), window.clearTimeout(i), window.clearTimeout(a), window.clearTimeout(n), $(t[0]).addClass("catalog-block__img_on").siblings().removeClass("catalog-block__img_on")
            }), $(".creative-vote").hover(function() {
                var e = 0;
                t = $(this).find(".creative-vote__media"), o = window.setInterval(function() {
                    e = e < t.length - 1 ? e + 1 : 0, $(t[e]).fadeIn(500).siblings(".creative-vote__media").fadeOut(500)
                }, 2e3)
            }, function() {
                window.clearInterval(o)
            }), $(function() {
                $header = $("@header"), $toggleSearch = $("@topSearchTrigger"), $body = $("body"), $searchBlock = $("@topSearchBlock"), $toggleSearch.on("click", function() {
                    return $header.hasClass("header_search-mode") ? ($toggleSearch.removeClass("top-search__link_close"), $header.removeClass("header_search-mode header_white"), $(window).trigger("scroll")) : ($toggleSearch.addClass("top-search__link_close"), $header.addClass("header_search-mode header_white"), setTimeout(function() {
                        $("@topSearchInput").focus()
                    }, 100)), !0
                }), getCover = function() {
                    return $body.hasClass("is-covered")
                }, addCover = function() {
                    getCover() || ($body.addClass("is-covered"), $header.addClass("header_covered"), $toggleSearch.on("click.fixedHeader", function() {
                        $toggleSearch.off("click.fixedHeader"), removeCover()
                    }))
                }, removeCover = function() {
                    getCover() && ($body.removeClass("is-covered"), $header.removeClass("header_covered"))
                }, $("@topSearchInput").on("input", function() {
                    inputLength = $(this).val().length, inputLength > 1 ? addCover() : removeCover()
                })
            }), $(function() {
                $body = $("body"), splat.getHashBlockName = function() {
                    return window.location.hash.slice(1) || !1
                }, splat.isModal = function(e) {
                    return splat.getBlock(e).is('[data-modal="true"]')
                }, splat.addCover = function() {
                    return $body.addClass("is-covered")
                }, splat.removeCover = function() {
                    return $body.removeClass("is-covered")
                }, splat.getBlockName = function(e) {
                    return e.attr("data-block")
                }, splat.getBlock = function(e) {
                    return $("@" + e)
                }, splat.getTrigger = function(e) {
                    return $('[data-block="' + e + '"]')
                }, splat.addHash = function(e) {
                    window.location.hash = e
                }, splat.removeHash = function() {
                    history.pushState("", document.title, window.location.pathname)
                }, splat.showBlock = function(e) {
                    console.log("show", e), splat.isModal(e) && splat.addCover(), $trigger = splat.getTrigger(e).addClass("is-active"), $block = splat.getBlock(e).addClass("is-visible"), "socialPopup" == e ? setTimeout(function() {
                        $popup = $block.parents(".popup.is-visible"), $popup[0] ? $popup.on("click.hideSocial", function() {
                            splat.hideBlock(e), $(".popup").off("click.hideSocial")
                        }) : $body.on("click.hideSocial", function() {
                            splat.hideBlock(e), $body.off("click.hideSocial")
                        })
                    }, 10) : (splat.addHash(e), $body.on("click.underlayer" + e, ".popup.is-visible", function() {
                        splat.hideBlock(e), $body.off("click.underlayer" + e)
                    }), $body.on("click.underlayer" + e, ".popup.is-visible .popup-block", function(e) {
                        e.stopPropagation()
                    }), $(document).on("keyup.underlayer" + e, function(t) {
                        27 == t.keyCode && (splat.hideBlock(e), $(document).off("keyup.underlayer" + e))
                    }))
                }, splat.hideBlock = function(e) {
                    if (console.log("hide", e), splat.removeHash(e), splat.removeCover(), splat.getTrigger(e).removeClass("is-active"), splat.getBlock(e).removeClass("is-visible"), "videoBlock" == e) {
                        var t = $("@videoBlock").find("iframe").first(),
                            o = t.attr("src");
                        t.attr("src", ""), t.attr("src", o)
                    }
                }, splat.toggleBlock = function(e) {
                    splat.getBlock(e).hasClass("is-visible") ? splat.hideBlock(e) : splat.showBlock(e)
                }, splat.getHashBlockName() && ($activeBlock = splat.getBlock(splat.getHashBlockName()), $activeBlock[0] && splat.showBlock(splat.getHashBlockName())), splat.getHashBlockName = function() {
                    return window.location.hash.slice(1)
                }, $body.on("click", "@showBlock", function() {
                    blockName = splat.getBlockName($(this)), splat.showBlock(blockName)
                }), $body.on("click", "@closeBlock", function() {
                    blockName = splat.getBlockName($(this)), splat.hideBlock(blockName)
                }), $body.on("click", "@toggleBlock", function() {
                    blockName = splat.getBlockName($(this)), splat.toggleBlock(blockName)
                }), $body.on("mouseenter mouseleave", "@toggleBlockHover", function() {
                    blockName = splat.getBlockName($(this)), splat.toggleBlock(blockName)
                })
            }), $(function() {
                $vacancy = $("@vacancy"), content = "@vacancy-content", $('@showBlock[data-block="vacancies"]').on("click", function() {
                    splat.addCover()
                }), $('@closeBlock[data-block="vacancies"]').on("click", function() {
                    splat.removeCover()
                }), $vacancy.on("click", function() {
                    $this = $(this), $content = $this.find(content), $this.hasClass("is-active") ? ($content.animate({
                        height: 0
                    }, 300), setTimeout(function() {
                        $this.removeClass("is-active")
                    }, 100)) : (autoHeight = $content.css("height", "auto").height(), $content.css("height", 0).animate({
                        height: autoHeight
                    }, 300), $this.addClass("is-active").siblings($vacancy).filter(".is-active").removeClass("is-active").find(content).animate({
                        height: 0
                    }, 300))
                })
            }), $(function() {
                $setMapTriggers = $("@showMap"), getMapName = function(e) {
                    return e.attr("data-map")
                }, getMapCenter = function(e) {
                    return "manufactureCanvas" == e ? {
                        lat: "58.371917",
                        "long": "33.300018"
                    } : {
                        lat: "55.75092",
                        "long": "37.583895"
                    }
                }, getMapZoom = function(e) {
                    return "manufactureCanvas" == e ? 7 : 15
                }, getMapOptions = function(e, t) {
                    return zoom = getMapZoom(e), {
                        center: t,
                        zoom: zoom,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        backgroundColor: "#ffffff"
                    }
                }, splat.setMap = function(e) {
                    canvas = $("@" + e)[0], coords = getMapCenter(e), center = new google.maps.LatLng(coords.lat, coords["long"]), options = getMapOptions(e, center), map = new google.maps.Map(canvas, options), marker = new google.maps.Marker({
                        position: center,
                        map: map
                    })
                }, $("@showMap").on("click", function() {
                    console.log("showMap click"), mapName = getMapName($(this)), setTimeout(function() {
                        splat.setMap(mapName)
                    }, 100)
                })
            }), $(function() {
                $(".btn_send-form").on("click", function() {
                    values = $(".questionary-form").serialize(), $.ajax({
                        url: "/creative/ajax.php?action=qform",
                        type: "POST",
                        data: values
                    }).done(function() {
                        $(".popup__close[data-block=questionaryBlock]").click()
                    }).fail(function() {}).always(function() {})
                })
            }), $(function() {
                $votes = $("@vote"), $votes.on("click", function(e) {
                    return $vote = $(this), $vote.hasClass("is-disabled") || $vote.hasClass("is-active") ? (e.preventDefault(), !1) : void $.ajax({
                        url: LANG_DIR + "creative/ajax.php?action=vote",
                        type: "POST",
                        dataType: "JSON",
                        data: {
                            voting: $("input#" + $vote.attr("for")).attr("name"),
                            variant: $("input#" + $vote.attr("for")).attr("value")
                        }
                    }).done(function(e) {
                        $(".vote__heart-counter", $vote).text(e.current_counter), $(".voting-top__amount", $vote.parent().prev()).text(e.sum_counter), $vote.addClass("is-active").siblings($votes).addClass("is-disabled")
                    }).fail(function() {}).always(function() {})
                })
            }), $(function() {
                $(".creative-form").on("submit", function(e) {
                    e.preventDefault(), $form = $(this), $.ajax({
                        url: "/creative/ajax.php?action=send",
                        type: "POST",
                        data: $form.serialize()
                    }).done(function() {
                        $(".creative-form")[0].reset()
                    }).fail(function() {}).always(function() {})
                })
            }), $(function() {
                $form = $("@reviewForm"), $form[0] && ($authorized = $("input[name=authorized]"), bAuthorized = !1, "Y" == $authorized.val() && (bAuthorized = !0), $(".rate-stars__item_review").on("click", function() {
                    var e;
                    e = $(".rate-stars__item_review.rate-stars__item_active").length, $("input[name=vote]").val(e)
                }), $("@sendOrAuth").on("click", function() {
                    var t;
                    t = !1, $("@reviewStateFirst").children(".input:visible").each(function(e, o) {
                        $(o).val().length < 2 ? ($(this).css({
                            border: "1px solid #d00"
                        }), t = !0) : $(this).removeAttr("style")
                    }), t || ($("@reviewStateFirst").removeClass("is-visible"), bAuthorized ? e($form) : $("@reviewStateChoice").addClass("is-visible"))
                }), $("@send").on("click", function() {
                    hasError = !1, emailAuthForm = $("@reviewStateRegistration"), emailAuthForm.is(":visible") && emailAuthForm.children(".input").each(function(e, t) {
                        if (0 === $(t).val().length || "email" === $(t).attr("name") && !validateEmail($(t).val())) {
                            if ($(t).hasClass("input_hp")) return;
                            $(t).css({
                                border: "1px solid #d00"
                            }), hasError = !0
                        } else $(t).removeAttr("style")
                    }), hasError || e($form)
                }), $("@authByEmail").on("click", function() {
                    $("@reviewStateChoice").removeClass("is-visible"), $("@reviewStateRegistration").addClass("is-visible")
                }), $($form, ".input:visible").on("keypress", function() {
                    void 0 !== $(this).attr("style") && $(this).removeAttr("style")
                }), $(window).on("hashchange", function() {
                    "#review" == window.location.hash && $("@firstTitle").val().length > 1 && e($form)
                }))
            }), $(function() {
                $header = $("@header"), $window = $(window), $window.on("scroll", function() {
                    return s = $window.scrollTop(), s >= 50 && $(window).width() > 1080 && $header.addClass("header_white"), s < 50 && !$header.hasClass("header_search-mode") && $header.removeClass("header_white"), !0
                }), setTimeout(function() {
                    $window.trigger("scroll")
                }, 50)
            }), $(function() {
                $("@select").ikSelect()
            }), $(function() {
                $(".pc-dropdown__selected").click(function() {
                    var e = $(this).closest(".pc-dropdown"),
                        t = e.find(".pc-dropdown__list");
                    e.is(".opened") ? (e.removeClass("opened"), t.hide(), $(".product-search-result").hide()) : (e.addClass("opened"), t.slideDown(), $(".product-search-result").slideDown())
                }), $(".pc-dropdown__item_input input").keyup(function() {
                    var e = $(this).val(),
                        t = $(this).closest(".pc-dropdown__item_input").find(".pc-dropdown__item_input__remove");
                    e.length > 0 ? t.show() : t.hide()
                }), $(".pc-dropdown__item_input__remove").click(function() {
                    $(this).closest(".pc-dropdown__item_input").find("input").val("")
                }), $(window).click(function(e) {
                    $(e.target).closest(".pc-dropdown").length || $(e.target).is(".pc-dropdown__selected") || ($(".pc-dropdown").removeClass("opened"), $(".pc-dropdown__list").hide(), $(".product-search-result").hide())
                })
            }), $(".product-header__link_about").click(function() {
                return $(this).toggleClass("on").find(".product-header__dropdown").toggleClass("on")
            }), $fotorama = $("@fotorama"), $fotorama[0] && $fotorama.on("fotorama:ready", function() {
                setTimeout(function() {
                    $arrows = $fotorama.find(".fotorama__arr"), $wrapper = $fotorama.find(".fotorama__stage").first(), $arrows.insertBefore($wrapper)
                }, 500)
            }).fotorama(), $(".fotorama").length > 0 && $(".fotorama").on("fotorama:fullscreenenter fotorama:fullscreenexit", function(e, t) {
                "fotorama:fullscreenenter" === e.type ? t.setOptions({
                    nav: !1
                }) : t.setOptions({
                    nav: "thumbs"
                })
            }).fotorama(), $(".rate-stars__item_review").click(function() {
                var e, t, o, i;
                for (i = $(".rate-stars__item_review"), i.removeClass("rate-stars__item_active"), $(this).addClass("rate-stars__item_active"), t = 0, o = []; t < i.length;) {
                    if ($(i[t]).hasClass("rate-stars__item_active")) {
                        e = t + 1;
                        break
                    }
                    $(i[t]).addClass("rate-stars__item_active"), o.push(t++)
                }
                return o
            }), $(".rate-stars__item_review").hover(function() {
                var e, t, o;
                for (o = $(".rate-stars__item_review"), o.removeClass("rate-stars__item_hover"), $(this).addClass("rate-stars__item_hover"), e = 0, t = []; e < o.length && !$(o[e]).hasClass("rate-stars__item_hover");) $(o[e]).addClass("rate-stars__item_hover"), t.push(e++);
                return t
            }), $(".rate-stars_review").hover(function() {}, function() {
                return $(".rate-stars__item_review").removeClass("rate-stars__item_hover")
            }), $(".review-block").each(function() {
                var e;
                return e = $(this).find(".review-block__content"), e.height() < 90 ? ($(this).find(".review-block__content").addClass("review-block__content_active"), $(this).find(".review-block__expand").hide()) : void 0
            }), $(".review-block__info").each(function() {
                var e = $(this),
                    t = e.find(".review-block__info-link"),
                    o = e.find(".popup-menu"),
                    i = e.find(".popup-menu__link_report"),
                    a = e.find(".popup-menu__link_copy");
                t.on("click", function(e) {
                    e.stopPropagation(), o.show(), $(document).on("click.popup", function(e) {
                        var t = $(e.target),
                            i = t.hasClass("review-block__info") ? t : t.parents(".review-block__info") > 0 ? t.find(".review-block__info") : null;
                        null == i && o.hide()
                    })
                });
                var s = new ZeroClipboard(a[0]);
                s.on("ready", function() {
                    s.on("aftercopy", function() {})
                }), a.on("click", function(e) {
                    e.preventDefault()
                }), i.on("click", function(e) {
                    e.preventDefault()
                })
            }), $(".series-catalog__holder").each(function() {
                "undefined" != typeof $().isotope && $(".series-catalog__holder").isotope({
                    layoutMode: "fitRows",
                    hiddenStyle: {
                        opacity: 0
                    },
                    visibleStyle: {
                        opacity: 1
                    },
                    transitionDuration: "0"
                })
            }), $(".series-tags__link").each(function() {
                var e = $(this),
                    t = e.closest(".series-tags"),
                    o = e.data("tag"),
                    i = ".tag-" + o,
                    a = !1;
                e.on("click", function(o) {
                    o.preventDefault();
                    var s = i;
                    a && e.hasClass("series-tags__link_active") ? (console.log(a), s = "*", t.removeClass("series-tags__filtered"), e.removeClass("series-tags__link_active"), a = !1) : ($(".series-tags__link_active").removeClass("series-tags__link_active"), t.addClass("series-tags__filtered"), e.addClass("series-tags__link_active"), a = !0), $(".series-catalog__holder").isotope({
                        filter: s
                    })
                })
            }), $(".series").each(function() {
                if ("undefined" != typeof $().isotope) {
                    var e = $(".series");
                    e.isotope({
                        layoutMode: "fitRows",
                        hiddenStyle: {
                            opacity: 0
                        },
                        visibleStyle: {
                            opacity: 1
                        },
                        transitionDuration: "0"
                    }).isotope("on", "layoutComplete", function() {
						var size = $(".catalog-transparent").find(".catalog-block").size();
						if (size % 2 != 0) {
							$(".catalog-transparent").find(".catalog-block").last().addClass("catalog-block_full");
						}
						
                        setTimeout(function() {
                            var e = $(".oral-care:visible").length,
                                t = $(".home-care:visible").length,
                                o = $(".baby-care:visible").length;
                            $("#oral-care-title")[e > 0 ? "show" : "hide"](300), $("#home-care-title")[t > 0 ? "show" : "hide"](300), $("#baby-care-title")[o > 0 ? "show" : "hide"](300);
                            var i = $("a:visible", $(".catalog-transparent__holder")).length,
                                a = $("a:visible", $(".catalog-colored__holder")).length;
                            $(".catalog-colored__title")[0 == i || 0 == a ? "hide" : "show"](300), $(".catalog-colored__desc")[0 == i || 0 == a ? "hide" : "show"](300)
                        }, 100)
                    })
                }
            });
			
			
			
			$(".catalog-header__item").each(function() {
                var e = $(this),
                    t = e.closest(".catalog-header"),
                    o = e.data("category"),
                    i = ".cat-" + o,
                    a = !1,
                    s = e.find(".catalog-header__item__icon"),
                    n = e.find(".catalog-header__icon-hover");
                s.attr("src").indexOf(".png") >= 0 && n.attr("src", s.attr("src").replace(".png", "-gray.png")), e.on("click", function(o) {
                    o.preventDefault();
                    var s = i;
                    e.hasClass("catalog-header__item_active") ? (s = "*", t.removeClass("catalog-header__filtered"), e.removeClass("catalog-header__item_active"), a = !1) : ($(".catalog-header__item_active").removeClass("catalog-header__item_active"), e.addClass("catalog-header__item_active"), t.addClass("catalog-header__filtered"), a = !0);
					
					$(".catalog-transparent").find(".catalog-block").removeClass("catalog-block_full");
					
					var size = $(".catalog-transparent").find(s).size();
					if (size % 2 != 0) {
						$(".catalog-transparent").find(s).last().addClass("catalog-block_full");
					}
					
					$(".series").isotope({
                        filter: s
                    })
                })
            }), $(".review-block__expand").click(function() {
                return $(this).hide().siblings(".review-block__content").addClass("review-block__content_active")
            }), setTimeout(function() {
                return $(".letter-scrolltop").addClass("on")
            }, 1e3), $(".letter-scrolltop__close, .letter-scrolltop__btn").click(function() {
                return $(this).parents(".letter-scrolltop").removeClass("on")
            }), $(".product-search-result").sticky({
                topSpacing: 90,
                getWidthFrom: ".map-points__insert"
            })
        })
    }.call(this), $(function() {
        $(".top-search").on("submit", function() {
            return !1
        })
    }), validateEmail = function(e) {
        var t;
        return t = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i, t.test(e)
    }, $(window).scroll(function() {
        var e = $(window).height(),
            t = $(window).scrollTop() + e;
        $(".animation-block").each(function() {
            var e = $(this).offset().top;
            t >= e && runAnimationBlock($(this))
        }), $(".animationRun").each(function() {
            var e = $(this).offset().top,
                o = $(this).data("animation-class"),
                i = $(this).data("animation-timestart"),
                a = $(this);
            t > e && (isNaN(i) ? $(this).addClass(o) : setTimeout(function() {
                a.addClass(o)
            }, i))
        })
    });
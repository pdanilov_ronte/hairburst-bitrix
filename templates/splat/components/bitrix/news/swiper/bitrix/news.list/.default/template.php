<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="facebook">
    <div class="facebook__holder">
        <div class="facebook__header">
            <div class="facebook__header-title">Наши новости</div>
        </div>
    </div>

    <div class="swiper-container facebook__scroller swiper-container-horizontal" data-role="facebookScroller">
        <div class="facebook__posts swiper-wrapper" data-role="facebookFeed"
             style="width: 4400px; transition-duration: 0ms; transform: translate3d(751.5px, 0px, 0px);">

            <? foreach ($arResult['ITEMS'] as $tiNum => $arItem): ?>

                <?
                if ($tiNum == 0) {
                    $tsClass = 'swiper-slide-active';
                } else if ($tiNum == 1) {
                    $tsClass = 'swiper-slide-next';
                } else {
                    $tsClass = 'is-disabled';
                }
                ?>

                <div class="swiper-slide facebook__item <?= $tsClass ?>" style="margin-right: 40px;">

                    <div class="facebook__wrapper fb-post">
                        <div class="facebook__content">
                            <div class="facebook__attachment"><a
                                    href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"
                                    target="_blank"><img class="attachment"
                                                         src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"></a>
                            </div>
                            <div class="facebook__text">
                                <h3 style="margin: 5px 0;"><? echo $arItem['NAME']; ?></h3>
                                <p class="facebook__p"><? echo $arItem["PREVIEW_TEXT"]; ?>
                                    <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"
                                       target="_blank" class="read-button"
                                       style="display:block;margin-top:10px">Читать далее</a></p></div>
                        </div>
                        <!--<div class="facebook__footer"></div>-->
                    </div>
                    <script>splat.facebookCounter = splat.facebookCounter || 0;
                        splat.facebookCounter++;</script>

                </div>

            <? endforeach; ?>

        </div>


        <div class="swiper-button swiper-button-next"></div>
        <div class="swiper-button swiper-button-prev is-disabled"></div>
        <div class="swiper-scrollbar">
            <div class="swiper-scrollbar-drag"
                 style="transition-duration: 0ms; transform: translate3d(0px, 0px, 0px); width: 532.491px;"></div>
        </div>
    </div>

    <div class="scroll__button scroll__button_left"></div>
    <div class="scroll__button scroll__button_right"></div>
</div>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<article class="product-header product-header_element" data-role="subheader productElement">
    <a href="/products/" class="product-header__item product-header__link_previous fixed-header__item"><i
            class="ico-arrow ico-arrow_left"></i>Наши продукты</a>
    <div class="product-header__item product-header__link_about">
        <?= $arResult["NAME"] ?><i class="ico-arrow ico-arrow_bottom"></i>
        <div class="product-header__dropdown">
            <div class="product-dropdown">
                <? $APPLICATION->IncludeComponent(
                    "splat:products.dropdown",
                    "",
                    Array(
                        'IBLOCK_ID' => 1,
                        'EXCLUDE_ID' => $arResult['ID'],
                        'SEF_FOLDER' => '/products/'
                    )
                );
                ?>
            </div>
        </div>
    </div>
    <div class="product-header__item">
    </div>
</article>

<article class="product-top">
    <div class="product-top__inner clearfix">

        <?

        CMedialib::Init();
        $arGalleryImages = array();

        foreach (CMedialibCollection::GetList() as $tItem) {
            if ($tItem['NAME'] == $arResult['PROPERTIES']['GALLERY_CODE']['VALUE']) {
                $intCollectionId = $tItem['ID'];
                break;
            }
        }

        if (isset($intCollectionId)) {
            $arColItems = CMedialibItem::GetList(array('arCollections' => array($intCollectionId)));
            foreach ($arColItems as $tItem) {
                $arGalleryImages[] = $tItem['PATH'];
            }
        }

        ?>

        <div
            data-width="580"
            data-height="280"
            data-nav="thumbs"
            data-thumbheight="65"
            data-thumbwidth="65"
            data-thumbmargin="20"
            data-allowfullscreen="true"
            data-click="false"
            data-swipe="false"
            data-loop="true"
            data-transition="crossfade"
            class="fotorama">

            <?
            foreach ($arGalleryImages as $tItem) {
                printf('<img src="%s">', $tItem);
            }
            ?>

        </div>

        <div class="product-top__info">
            <h1 class="product-top__info__title"><?= $arResult["NAME"] ?></h1>

            <div class="product-top__info__desc"><?= $arResult['PROPERTIES']["DESCRIPTION"]['~VALUE']['TEXT'] ?></div>

            <a href="/shopping-map/" class="product-top__btn btn btn_red">УЗНАТЬ ГДЕ КУПИТЬ</a>
            <!--<a href="#" class="product-top__btn btn btn_red product-top__btn_shop"
               target="_blank">КУПИТЬ В ИНТЕРНЕТ МАГАЗИНЕ</a>-->
        </div>

    </div>
</article>

<article class="product-facts">
    <div class="product-facts__inner clearfix">
        <div class="product-facts__left">
            <div class="product-facts__title">Назначение</div>
            <div class="product-facts__block"><?= $arResult['PROPERTIES']["PRESCRIPTION"]['~VALUE']['TEXT'] ?></div>
        </div>
        <div class="product-facts__right">
            <div class="product-facts__title">Ингредиенты</div>
            <div class="product-facts__block"><?= $arResult['PROPERTIES']["INGREDIENTS"]['~VALUE']['TEXT'] ?></div>
        </div>
    </div>
</article>

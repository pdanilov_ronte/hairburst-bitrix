<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="catalog-transparent">
    <div class="catalog-transparent__holder series" style="height:<?= 3 + (intval(count($arResult['ITEMS'])/2) + 1) * 438 ?>px;">

        <? $tiTop = 3; ?>
        <? foreach ($arResult['ITEMS'] as $tiIndex => $arItem): ?>
            <? $tiLeft = $tiIndex%2 === 0 ? 0 : 622; ?>

            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="catalog-block  catalog-block_hovered cat-390" style="position: absolute; left: <?=$tiLeft?>px; top: <?=$tiTop?>px;">
                <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" class="catalog-block__img catalog-block__img_on" width="200"
                     style="margin-left: 80px !important; margin-top: 35px !important">
                <div class="catalog-block__inner">
                    <span class="catalog-block__title"><? echo $arItem["NAME"] ?></span>
                    <div class="catalog-block__separate"></div>
                    <p class="catalog-block__info"><? echo $arItem['PROPERTIES']['SHORT_DESCRIPTION']['VALUE'] ?></p>
                </div>
            </a>

            <? if (($tiIndex+1)%2 == 0) $tiTop += 438; ?>
        <? endforeach; ?>

    </div>
</div>

<!--<div class="series-catalog" style="display: none">
    <div class="series-catalog__holder">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="series-catalog-block tag-723 tag-1451 tag-721" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <img
                    class="series-catalog-block__img"
                    src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                    alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                    title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                />
                <span class="series-catalog-block__title"><? echo $arItem["NAME"] ?></span>
                <p class="series-catalog-block__desc"><br/>
                </p>
            </a>
        <? endforeach; ?>
    </div>
</div>-->

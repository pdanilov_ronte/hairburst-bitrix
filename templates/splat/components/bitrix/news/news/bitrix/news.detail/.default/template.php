<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style>
    section.production h3 {
        margin: 10px 0;
    }
    section.production p {
        margin: 10px 0;
    }
</style>
<section class="production">
    <div class="production-top"
         style="background-image:<?= !empty($arResult['DETAIL_PICTURE']['SRC']) ? sprintf('url(%s)', $arResult['DETAIL_PICTURE']['SRC']) : 'none' ?>">
    </div>
    <article class="production-content">
        <div class="production-content__holder">
            <h2 class="production-content__heading"><?= $arResult["NAME"] ?></h2>
            <span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
            <div><?= $arResult["DETAIL_TEXT"]; ?></div>
        </div>
    </article>
</section>

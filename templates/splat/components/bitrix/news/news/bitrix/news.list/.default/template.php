<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="team-top" style="background-image: none;">
</div>


<div class="expert-inner clearfix">

    <div class="expert-content">

        <!--<form class="expert-search">
            <input class="expert-search__input" placeholder="Воспользуйтесь поиском"
                   type="text"/>
            <button class="expert-search__btn">Найти новость</button>
        </form>-->

        <? foreach ($arResult["ITEMS"] as $tNum => $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>

            <div class="expert-advice expert-advice_active" data-section="35"
                 id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                <span class="expert-advice__counter"><?= $tNum + 1 ?></span>
                <span class="expert-advice__title">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                    <span class="news-date-time"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
                </span>
                <div class="expert-advice__content"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
            </div>

        <? endforeach; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
            <br/><?= $arResult["NAV_STRING"] ?>
        <? endif; ?>

    </div>

</div>

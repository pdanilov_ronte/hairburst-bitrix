<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mfeedback">
    <? if (!empty($arResult["ERROR_MESSAGE"])) {
        foreach ($arResult["ERROR_MESSAGE"] as $v)
            ShowError($v);
    }
    if (strlen($arResult["OK_MESSAGE"]) > 0) {
        ?>
        <div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div><?
    }
    ?>

    <form action="<?= POST_FORM_ACTION_URI ?>" method="POST" class="contact-form" data-role="contact-form">
        <?= bitrix_sessid_post() ?>
        <input type="text" placeholder="Ваше имя" class="input input__text" data-role="contact-form-name"
               name="user_name" value="<?= $arResult["AUTHOR_NAME"] ?>">
        <input placeholder="Ваш email, для ответа" class="input input__text" name="user_email"
               value="<?= $arResult["AUTHOR_EMAIL"] ?>">

        <textarea placeholder="Текст сообщения" class="input input__textarea"
                  name="MESSAGE"><?= $arResult["MESSAGE"] ?></textarea>

        <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
        <input class="btn btn_red" type="submit" name="submit" value="Отправить"/>
    </form>
</div>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
		continue;
?>
	<?if($arItem["SELECTED"]):?>
        <a href="<?=$arItem["LINK"]?>" class="top-nav__item top-nav__item_active">
            <span class="top-nav__item__inner"><?=$arItem["TEXT"]?></span>
        </a>
	<?else:?>
        <a href="<?=$arItem["LINK"]?>" class="top-nav__item">
            <span class="top-nav__item__inner"><?=$arItem["TEXT"]?></span>
        </a>
	<?endif?>

<?endforeach?>
<?endif?>
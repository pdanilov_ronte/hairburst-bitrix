<footer class="footer">
    <div class="footer__inner clearfix">

        <div class="footer__left">
            <div class="footer-menu footer-menu_marked">
                <a class="footer-menu__link footer-menu__title" href="/contacts/">Контакты</a>
                <ul class="footer-list">
                    <li class="footer-list__item">
                        <span class="footer-menu__link footer-menu__link_marked">Главная улица, д.1, бизнес-центр «Прайм», 77 этаж</span>
                    </li>
                    <li class="footer-list__item">+7 (495) 123 45 67</li>
                    <li class="footer-list__item">
                        <a class="footer-menu__link footer-menu__link_marked" href="mailto:info@hairburst.ru">info@hairbrst.ru</a>
                    </li>
                </ul>
            </div><!-- /footer-menu -->
        </div><!-- /footer__right -->

        <div class="footer__center">
            <div class="footer-menu">

                <? $APPLICATION->IncludeComponent(
                    "splat:products.footer",
                    "",
                    Array(
                        'IBLOCK_ID' => 1,
                        'SEF_FOLDER' => '/products/'
                    )
                );
                ?>

            </div>
            <div class="footer-menu">
                <div class="footer-menu__link footer-menu__title">Публикации</div>
                <ul class="footer-list">
                    <?
                    $APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", Array(
                        "ROOT_MENU_TYPE" => "bottom",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "N",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "N",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => "",
                    ),
                        false
                    );
                    ?>
                </ul>
            </div>
        </div><!-- /footer__center -->

        <div class="footer__right">
            <ul class="social-bar clearfix">
                <li class="social-bar__item"><a target="_blank" href="#"
                                                class="ico-social-footer ico-social-footer_fb"></a></li>
                <li class="social-bar__item"><a target="_blank" href="#"
                                                class="ico-social-footer ico-social-footer_inst"></a></li>
                <li class="social-bar__item"><a target="_blank" href="#"
                                                class="ico-social-footer ico-social-footer_vk"></a></li>
            </ul>
            <span class="footer__copyright">© 2016 HAIRBURST</span>

        </div>
        <!-- /footer__left -->

    </div><!-- /footer__inner -->
</footer>

<!-- GA script placeholder -->
<!-- /GA script placeholder -->

<!-- Yandex.Metrika counter -->
<!-- /Yandex.Metrika counter -->

</body>
</html>